import { FaHome } from "react-icons/fa";
import { HiUserGroup } from "react-icons/hi";
import { BiCalendar } from "react-icons/bi";

const NavBar = () => {
    return (
        <div className='navbar'>
           <nav>
            <ul>
                <li><FaHome /> <span>Home</span> </li>
                <li className='active'><HiUserGroup /> Personnel List</li>
                <li><BiCalendar /> Daily Attendance</li>
            </ul>
           </nav>
        </div>
    )
}

export default NavBar
