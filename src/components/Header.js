
const Header = ({logo}) => {
    return (
        <>
        <img className='logo' src={logo} alt="logo" />
        <div className='user'>
            <p>Hallo, </p>
            <p style={{color: '#23C7C6'}}>Gadjian User</p>
            <div className='circle'>Photo</div>
        </div>
        </>
        
    )
}

export default Header
