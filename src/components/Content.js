import { Input, Button, Icon, Card } from 'semantic-ui-react'
import { FaEllipsisH } from 'react-icons/fa'
import PaginationList from 'react-pagination-list'

const Content = ({personnel, findPersons}) => {
    return (
        <div className='container'>
            <div className='title'>
                <div className='title-text'>
                    <h2>Personnel List</h2>
                    <h4>List of all personnel</h4>
                </div>
                <Input icon='search' type='text' iconPosition='left' placeholder="Find Personnels" onChange={event => findPersons(event.target.value)} />
                <Button color='teal' icon labelPosition='right'>
                    Add Personnels
                    <Icon name='plus' />
                </Button>
            </div>
            <div className='detail'>
                <Card.Group>
                    <PaginationList layout='horizontal' data={personnel} pageSize={4} renderItem={
                       (item, key) => (
                        <Card key={key} style={{width:'220px'}}>
                            <Card.Header className='card-header'>
                                Personnel ID : <>{item.id['value']}</> <FaEllipsisH style={{float:'right'}} />
                            </Card.Header>
                            <Card.Content className='card-body'>
                                <img className='person-pic' alt='foto' src={item.picture['large']} />
                                <strong>Name</strong>
                                <p>{item.name['first']} {item.name['last']}</p>
                                <strong>Telephone</strong>
                                <p>{item.phone}</p>
                                <strong>Birthday</strong>
                                <p>{item.dob['date']}</p>
                                <strong>Email</strong>
                                <p>{item.email}</p>
                            </Card.Content>
                        </Card>
                       ) 
                    } />
                </Card.Group>
            </div>
        </div>
    )
}
export default Content
