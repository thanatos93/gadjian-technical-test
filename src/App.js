import Header from './components/Header'
import NavBar from './components/NavBar'
import Content from './components/Content'
import logo from './logo.png'
import { useState, useEffect } from 'react'

function App() {
  const [person, setPerson] = useState([])

  useEffect(() => {
    const getPersons = async () => {
      const personFromServers = await fetchPersons()
      setPerson(personFromServers)
    }

    getPersons()

  }, [])

  const fetchPersons = async () => {
    const res = await fetch('https://randomuser.me/api/?results=28')
    const data = await res.json()

    return data.results
  }

  const findPersonByName = (value) => {
    
  }

  return (
    <div className="App">
      <header>
        <Header logo={logo} />
      </header>
      
      <div className='content-wrap'>
        <NavBar />
        <Content personnel={person} findPersons={findPersonByName} />
      </div>
    </div>
  );
}

export default App;
